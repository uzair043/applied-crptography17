#!/usr/bin/env python
import sys   # do not use any other imports/libraries

# It took me too much time to figure it out

def bytestring_to_int(s):
    # your implementation here
    i = 0
    for letter in  range(0, len(s) ):
        if(letter == 0):
            i = ord(s[letter])
        else:
            i = i << 8
            i = i | ord(s[letter])
    return i

def int_to_bytestring(i):
    byteString = ""
    while i:
    	n = i & 0xff
    	byteString = chr(n) + byteString
    	i = i >> 8

    if byteString == "":
        byteString = chr(0x00)
    return byteString


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der

# figure out what to put in '...' by looking on ASN.1 structure required (see slides)
asn1 = asn1_tag_explicit(asn1_sequence(asn1_set(asn1_integer(5) + asn1_tag_explicit(asn1_integer(200), 2) +
asn1_tag_explicit(asn1_integer(65407), 11)) + asn1_boolean(True) +
asn1_bitstring("110") +
asn1_octetstring("\x00\x01\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02\x02") +
asn1_null() +
asn1_objectidentifier([1, 2, 840, 113549, 1]) +
asn1_printablestring("hello.") +
asn1_utctime("150223010900Z")), 0)

open(sys.argv[1], 'w').write(asn1)

