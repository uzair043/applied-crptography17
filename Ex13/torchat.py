#!/usr/bin/python

#apt-get install python-socksipy

import argparse
import socks
import socket
import sys
import random
import re
import time
# do not use any other imports/libraries

# It took me 4 hours

# parse arguments
parser = argparse.ArgumentParser(description='TorChat client')
parser.add_argument('--myself', required=True, type=str, help='My TorChat ID')
parser.add_argument('--peer', required=True, type=str, help='Peer\'s TorChat ID')
args = parser.parse_args()

# route outgoing connections through Tor
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket


# reads and returns torchat command from the socket
def read_torchat_cmd(s):
    cmd = ""
    buff = ""

    while buff != '\n':
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        cmd = cmd + buff

    return cmd

print "[+] Connecting to peer", args.peer

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((args.peer + ".onion", 11009))

random_biscuit = random.randint(10000000000000000000, 18446744073709551616)

cmd = "ping " + args.myself + " " + str(random_biscuit)

print "[+] Sending:", cmd
s.send(cmd + '\n')

print "[+] Listening..."
ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
ss.bind(('', 8888))
ss.listen(0)

(socket_in, address) = ss.accept()

print "[+] Client %s:%s" % (address[0], address[1])

status_received = False
while True:
    cmdr = read_torchat_cmd(socket_in)
    print "[+] Received:", cmdr[:-1]

    commandArguments = cmdr.split()

    message = commandArguments[0]

    if message == 'ping':
        received_biscuit = commandArguments[2]
    elif message == 'pong':
        cmd = "pong " + str(received_biscuit)
        print "[+] Sending:", cmd
        s.send(cmd + '\n')
    elif message == 'status':
        if not status_received:
            cmd = "add_me"
            status_received = True
            print "[+] Sending:", cmd
            s.send(cmd + '\n')
            cmd = "status available"
            print "[+] Sending:", cmd
            s.send(cmd + '\n')
            cmd = "profile_name Uzair"
            print "[+] Sending:", cmd
            s.send(cmd + '\n')
        cmd = "status available"
        print "[+] Sending:", cmd
        s.send(cmd + '\n')
    elif message == 'message':
        print "[+] Enter Message: ",
        mesg = raw_input()
        cmd = "message " + mesg
        print "[+] Sending:", cmd
        s.send(cmd + '\n')
