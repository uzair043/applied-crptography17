#!/usr/bin/env python

import argparse, hashlib, sys, datetime # do not use any other imports/libraries


# It took me 6 hours


# parse arguments
parser = argparse.ArgumentParser(description='Proof-of-work solver')
parser.add_argument('--difficulty', default=0, type=int, help='Number of leading zero bits')
args = parser.parse_args()


# converts integer to bytes (big endian)
def nb(i, length=0):
    bytes = ""
    for smth in xrange(length):
        bytes = chr(i & 0xff) + bytes
        i >>= 8
    return bytes

counter = -1

bits_hash = ""

start_time = datetime.datetime.utcnow()

challenge_solved = False

#Make compare references
complete_zero_bytes = (args.difficulty / 8)
zero_bytes = complete_zero_bytes * '\x00'

additional_zero_bits = args.difficulty % 8
bitstring = (additional_zero_bits * '0') + ((8 - additional_zero_bits) * '1')
int_rep = int(bitstring, 2)

while not challenge_solved:
    counter = counter + 1
    nonce = nb(counter, 8)

    input_value = "Uzair" + nonce

    hash_value = hashlib.sha256(hashlib.sha256(input_value).digest()).digest()

    if hash_value[0:complete_zero_bytes] == zero_bytes:

        byte_value = ord(hash_value[complete_zero_bytes])

        if byte_value <= int_rep:
            end_time = datetime.datetime.utcnow()
            seconds = (end_time - start_time).total_seconds()
            print '[+] Solved in ' + str(seconds) + ' sec (' + str((counter / 1000000.0) / seconds) + ' Mhash/sec)'
            print '[+] Input: ' + input_value.encode('hex')
            print '[+] Solution: ' + hash_value.encode('hex')
            print '[+] Nonce: ' + str(counter)
            challenge_solved = True

#Difficulty = 26
#Name = Uzair
#[+] Solved in 92.108466 sec (0.228738821902 Mhash/sec)
#[+] Input: 557a6169720000000001417bee
#[+] Solution: 000000209c71bab6a23515179d63b85bf395d87cf43bf8f50bf18bc9ce4cdbb0
#[+] Nonce: 21068782


