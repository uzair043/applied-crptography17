
#!/usr/bin/env python
import os, sys       # do not use any other imports/libraries
# specify here how much time (in hours) your solution required

def bytestring_to_int(s):
    # your implementation here
    i = 0
    for letter in  range(0, len(s) ):
        if(letter == 0):
            i = ord(s[letter])
        else:
            i = i << 8
            i = i | ord(s[letter])
    return i

def int_to_bytestring(i, length):
    # your implementation here
    s = ""
    for foo in xrange(length):
        s = chr(i & 255) + s
        i = i >> 8
    return s


def encrypt(pfile, kfile, cfile):
    # your implementation here
    pfile = open(sys.argv[2]).read()        #reading the plain text file
    plainFileInteger = bytestring_to_int(pfile)     #converting the plain text file into one big integer

    kfile = os.urandom(len(pfile))  #generating random key as of the length of the plain text file
    keyFileInteger = bytestring_to_int(kfile)   #converting the key to big integer

    cfile =  plainFileInteger ^ keyFileInteger  #generating the cipher text by XOR'ing the plain and key integers


    keyFileInteger = int_to_bytestring(keyFileInteger, len(kfile))  #converting the key integer to bytestring

    #saving the key
    saveKey = open(sys.argv[3], 'w')
    for i in range(0,len(keyFileInteger)):
        saveKey.write(keyFileInteger[i])

    #saving the cipher text
    cfile = int_to_bytestring(cfile, len(pfile))
    f = open(sys.argv[4], 'w');
    for i in cfile:
        f.write(i)

    pass


def decrypt(cfile, kfile, pfile):
    # your implementation here
    cfile = open(sys.argv[2]).read()    #reading the cipher text from file argument
    kfile = open(sys.argv[3]).read()    #reading the key file from argument

    cipherInteger = bytestring_to_int(cfile)    #converting the cipher text to integer
    keyInteger = bytestring_to_int(kfile)       #converting the key to integer

    pfile = cipherInteger ^ keyInteger          #getting the big integer for plain text by XOR'ing the cipher and key integers

    plaintText = int_to_bytestring(pfile, len(cfile))   #converting plain integer to plain text

    #writing the plain text to file
    f = open(sys.argv[4], 'w')
    f.write(plaintText)

    pass

def usage():
    print "Usage:"
    print "encrypt <plaintext file> <output key file> <ciphertext output file>"
    print "decrypt <ciphertext file> <key file> <plaintext output file>"
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
