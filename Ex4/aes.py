#!/usr/bin/python

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python-crypto
sys.path = sys.path[1:] # removes script directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Protocol.KDF import PBKDF2 # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Protocol.KDF-module.html#PBKDF2
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
import hashlib, hmac # do not use any other imports/libraries


# It took almost 16 hours :D

#-------------------Functions to convert to big integer and bytestring-------------------#
def bytestring_to_int(s):
    # your implementation here
    i = 0
    for letter in  range(0, len(s) ):
        if(letter == 0):
            i = ord(s[letter])
        else:
            i = i << 8
            i = i | ord(s[letter])
    return i

def int_to_bytestring(i):
    byteString = ""
    while i:
    	n = i & 0xff
    	byteString = chr(n) + byteString
    	i = i >> 8

    if byteString == "":
        byteString = chr(0x00)
    return byteString
############################################################################################



#------------------ASN.1 Encoder Functions-----------------------------------------------#
def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der
######################################################################################################




# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():

    salt = os.urandom(8)
    # measure time for performing 10000 iterations
    start_time = datetime.datetime.now()
    PBKDF2("password", salt, 10000, 36)
    stop_time = datetime.datetime.now()

    total_time = (stop_time - start_time).total_seconds()
    # extrapolate to 1 second

    iter = int(10000/(total_time/1))

    print "[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter)

    return iter # returns number of iterations that can be performed in 1 second


def encrypt(pfile, cfile):

    # benchmarking
    iter = benchmark()

    # asking for password
    print "[?] Enter password: "
    password = raw_input()

    # derieving key
    salt = os.urandom(8)
    derived_key = PBKDF2(password, salt, 36, iter)
    aes_key_128bit = derived_key[:16]
    HMAC_SHA1_Key = derived_key[16:]

    # writing ciphertext in temporary file and calculating HMAC digest
    IV = os.urandom(16)
    file_reader = open(pfile, 'rb')
    temp_reader = open('tempfile', 'w').close()
    temp_reader = open('tempfile', 'a')

    plain_text_block = file_reader.read(16)
    cipher_text_block = IV
    cipher = AES.new(aes_key_128bit)

    padding = False

    while plain_text_block:
        if(len(plain_text_block) != 16):
            difference = 16 - len(plain_text_block)
            if difference > 0:
                padding_char = chr(difference)
                padding = True

            for i in xrange(difference):
                plain_text_block = plain_text_block + padding_char


        xor = strxor(plain_text_block, cipher_text_block)

        cipher_text_block = cipher.encrypt(xor)
        temp_reader.write(cipher_text_block)
        plain_text_block = file_reader.read(16)

    if padding == False:
        plain_text_block = chr(16)*16
        xor = strxor(plain_text_block, cipher_text_block)
        cipher_text_block = cipher.encrypt(xor)
        temp_reader.write(cipher_text_block)

    file_reader.close()
    temp_reader.close()

    # writing DER structure in cfile

    hash = hmac.new(HMAC_SHA1_Key, None, hashlib.sha1)
    file_reader = open("tempfile", 'rb')
    data = file_reader.read(512)

    while data:
        hash.update(data)
        data = file_reader.read(512)

    parameters = asn1_sequence(asn1_sequence(asn1_octetstring(salt) + asn1_integer(iter) + asn1_integer(36)) +
                 asn1_sequence(asn1_objectidentifier([2,16,840,1,101,3,4,1,2]) + asn1_octetstring(IV)) +
                 asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) +
                 asn1_octetstring(hash.digest())))

    cipher_file_reader = open(cfile, 'w')
    cipher_file_reader.write(parameters)
    cipher_file_reader.close()

    # writing temporary ciphertext file to cfile

    temp_reader = open("tempfile", 'r')
    cipher_file_reader = open(cfile, 'a')
    data = temp_reader.read(512)

    while data:
        cipher_file_reader.write(data)
        data = temp_reader.read(512)

    # deleting temporary ciphertext file

    os.remove("tempfile")
    pass


def decrypt(cfile, pfile):


    # reading DER structure
    cipher_file_reader = open(cfile, 'rb')
    cipher_file_reader.read(1)
    DER_length = bytestring_to_int(cipher_file_reader.read(1))

    if DER_length >= 128:
        DER_length = bytestring_to_int(DER_length) & 127
        first_bytes = cipher_file_reader.read(DER_length)
        DER_length = 0
        for c in first_bytes:
            DER_length = (DER_length << 8) + ord(c)

    cipher_file_reader.close()
    cipher_file_reader = open(cfile, 'rb')

    parameters = cipher_file_reader.read(DER_length + 2)
    cipher_file_reader.close()

    decode_Parameters = decoder.decode(parameters)

    pbkdf2 = decode_Parameters[0][0]
    aesInfo = decode_Parameters[0][1]
    digestInfo = decode_Parameters[0][2]

    salt = str(pbkdf2[0])
    iter = int(pbkdf2[1])
    key_length = int(pbkdf2[2])

    cipher_identifier = str(digestInfo[0][0])
    IV = str(aesInfo[1])

    algo_identifier = str(digestInfo[0][0])
    HMAC_Digest = str(digestInfo[1])

    # asking for password
    print "[?] Enter Password: "
    password = raw_input()

    # derieving key
    derived_key = PBKDF2(password, salt, 36, iter)
    aes_key_128 = derived_key[:16]
    HMAC_SHA1_Key = derived_key[16:]

    # first pass over ciphertext to calculate and verify HMAC

    if algo_identifier == "1.3.14.3.2.26":
        hash = hmac.new(HMAC_SHA1_Key, None, hashlib.sha1)

        cipher_file_reader = open(cfile, 'rb')
        cipher_file_reader.read(DER_length + 2)
        cipher_data = cipher_file_reader.read(512)

        while cipher_data:
            hash.update(cipher_data)
            cipher_data = cipher_file_reader.read(512)

        calculted_digest = hash.digest()

        if calculted_digest != HMAC_Digest:
            print "[-] HMAC verification failure: wrong password or modified ciphertext!"
            return
        else:
            print "[+] HMAC verification successful!"
    else:
        print "[-] Unknown function used for HMAC encoding!"

    # second pass over ciphertext to decrypt
    decrypt_file_reader = open(pfile, 'a')
    cipher_file_reader = open(cfile, 'r')
    cipher_file_reader.seek(DER_length + 2)

    cipher = AES.new(aes_key_128)

    cipher_text_block = cipher_file_reader.read(16)
    xor_block = IV

    while cipher_text_block:
        next_cipher_block = cipher_file_reader.read(16)
        decrypt_text_block = cipher.decrypt(cipher_text_block)
        plain_text_block = strxor(decrypt_text_block, xor_block)

        if not len(next_cipher_block):
            c = ord(plain_text_block[-1])
            plain_text_block = plain_text_block[0:16-c]

        decrypt_file_reader.write(plain_text_block)
        xor_block = cipher_text_block
        cipher_text_block = next_cipher_block

    decrypt_file_reader.close()

    pass

def usage():
    print "Usage:"
    print "-encrypt <plaintextfile> <ciphertextfile>"
    print "-decrypt <ciphertextfile> <plaintextfile>"
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
