#!/usr/bin/python

import argparse, hmac, socket, sys, time, os, urlparse, datetime
from hashlib import md5, sha1
from Crypto.Cipher import ARC4
from pyasn1.codec.der import decoder  # do not use any other imports/libraries

# It took me 6 hours

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.0 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def get_pubkey_certificate(cert):
    # reads the certificate and returns (n, e)

    server_cert_DER = decoder.decode(cert)

    open("temp_file", 'w').write(cert)

    bit_string = server_cert_DER[0][0][6][1]

    octets_length = len(bit_string) / 8
    octets = 0

    for bit in bit_string:
        octets = octets << 1 | bit

    public_key = decoder.decode(int_to_bytestring(octets, octets_length))[0]

    return int(public_key[0]), int(public_key[1])

def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate byte size of the modulus n
    size = 0
    while n:
        size = size + 1
        n = n >> 8

    textLength = len(plaintext) + 3 + 8
    additionalPad = size - textLength
    paddingLength = 8 + additionalPad

    # plaintext must be at least 11 bytes smaller than modulus
    randomPadding = os.urandom(paddingLength)

    while '\x00' in randomPadding:
        randomPadding = randomPadding.replace('\x00', os.urandom(1))

    # generate padding bytes
    padded_plaintext = '\x00' + '\x02' + randomPadding + '\x00' + plaintext
    return padded_plaintext

def rsa_encrypt(cert, m):
    # encrypts message m using public key from certificate cert

    n, e = get_pubkey_certificate(cert)

    paddedText = pkcsv15pad_encrypt(m, n)

    m = bytestring_to_int(paddedText)

    c = pow(m, e, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    cipherText = int_to_bytestring(c, size)

    return cipherText

# converts bytes (big endian) to integer
def bn(bytes):
        num = 0
        for byte in bytes:
                num <<= 8
                num |= ord(byte)
        return num

# converts integer to bytes (big endian)
def nb(i, length=0):
    bytes = ""
    for smth in xrange(length):
        bytes = chr(i & 0xff) + bytes
        i >>= 8
    return bytes

# returns TLS record that contains client_hello handshake message
def client_hello():
    global client_random, handshake_messages

    print "--> client_hello()"
    layer = "\x03\x01"
    client_random = nb(int(time.time()), 4) + os.urandom(28)
    layer += client_random
    layer += nb(0, 1)

    # list of cipher suites the client supports
    csuite = "\x00\x05" # TLS_RSA_WITH_RC4_128_SHA

    layer += nb(len(csuite), 2)
    layer += csuite
    layer += nb(1, 1)
    layer += "\x00"

    # add handshake message header
    record = nb(1, 1) + nb(len(layer), 3) + layer

    handshake_messages += record

    # add record layer header
    record = "\x16" + "\x03\x01" + nb(len(record), 2) + record

    return record

# returns TLS record that contains client_key_exchange message containing encrypted pre-master secret
def client_key_exchange():
    global server_cert, premaster, handshake_messages, tls_version

    print "--> client_key_exchange()"

    premaster = tls_version + os.urandom(46)

    encrypted_premaster = rsa_encrypt(server_cert, premaster)

    record = nb(len(encrypted_premaster), 2) + encrypted_premaster

    # add handshake message header
    record = nb(16, 1) + nb(len(record), 3) + record

    handshake_messages += record

    # add record layer header
    record = "\x16" + "\x03\x01" + nb(len(record), 2) + record

    return record

# returns TLS record that contains change_cipher_spec message
def change_cipher_spec():
    print "--> change_cipher_spec()"

    record = "\x01"

    # add record layer header
    record = "\x14" + "\x03\x01" + nb(len(record), 2) + record

    return record

# returns TLS record that contains encrypted finished handshake message
def finished():
    global handshake_messages, master_secret

    print "--> finished()"
    client_verify = PRF(master_secret, "client finished", md5(handshake_messages).digest()+sha1(handshake_messages).digest(), 12)

    record = encrypt(("\x14" + nb(len(client_verify), 3) + client_verify), "\x16", "\x03\x01")

    handshake_messages += "\x14" + nb(len(client_verify), 3) + client_verify
    # add record layer header
    record = "\x16" + "\x03\x01" + nb(len(record), 2) + record

    return record

# returns TLS record that contains encrypted application data
def application_data(data):
    print "--> application_data()"
    print data.strip()

    encrypted_data = encrypt(data, "\x17", "\x03\x01")

    record = "\x17" + "\x03\x01" + nb(len(encrypted_data), 2) + encrypted_data

    return record

# parse TLS handshake messages
def parsehandshake(r):
    global server_hello_done_received, server_random, server_cert, handshake_messages, server_change_cipher_spec_received, server_finished_received, tls_version

    # decrypt if encryption enabled
    if server_change_cipher_spec_received:
        r = decrypt(r, "\x16", "\x03\x01")

    # read handshake message type and length from message header
    htype, hlength = r[0], bn(r[1:4])

    body = r[4:4+hlength]
    handshake = r[:4+hlength]
    handshake_messages+= handshake

    if htype == "\x02":
        print "	<--- server_hello()"

        tls_version = body[0:2]

        server_random = body[2:34]
        gmt = datetime.datetime.fromtimestamp(bn(body[2:6])).strftime('%Y-%m-%d %H:%M:%S')

        sessidLength = bn(body[34])
        sessid = body[35:sessidLength+35]
        cipher = body[sessidLength+35:sessidLength+37]
        compression = body[sessidLength+37]

        print " [+] server randomness:", server_random.encode('hex').upper()
        print " [+] server timestamp:", gmt
        print " [+] TLS session ID:", sessid.encode('hex').upper()

        if cipher=="\x00\x2f":
            print " [+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA"
        elif cipher=="\x00\x35":
            print " [+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA"
        elif cipher=="\x00\x39":
            print " [+] Cipher suite: TLS_DHE_RSA_WITH_AES_256_CBC_SHA"
        elif cipher=="\x00\x05":
            print " [+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA"
        else:
            print "[-] Unsupported cipher suite selected:", cipher.encode('hex')
            sys.exit(1)

        if compression!="\x00":
            print "[-] Wrong compression:", compression.encode('hex')
            sys.exit(1)

    elif htype == "\x0b":
        print "	<--- certificate()"

        certificates = r[4:hlength+4]

        certificatesLength = bn(certificates[0:3])
        certificatesData = certificates[3:certificatesLength+3]

        certlen = bn(certificatesData[0:3])

        print " [+] Server certificate length:", certlen

        server_cert = certificatesData[3:certlen+3]

        if args.certificate:
            open(args.certificate, 'w').write('-----BEGIN CERTIFICATE-----\n' + server_cert.encode("base64") + '-----END CERTIFICATE-----')
            print " [+] Server certificate saved in:", args.certificate

    elif htype == "\x0e":
        print "	<--- server_hello_done()"
        server_hello_done_received = True

    elif htype == "\x14":
        print "	<--- finished()"
        # hashmac of all handshake messages except the current finished message (obviously)
        verify_data_calc = PRF(master_secret, "server finished", md5(handshake_messages[:-4-hlength]).digest()+sha1(handshake_messages[:-4-hlength]).digest(), 12)
        server_verify = body
        if server_verify!=verify_data_calc:
            print "[-] Server finished verification failed!"
            sys.exit(1)
        server_finished_received = True
    else:
        print "[-] Unknown Handshake Type:", htype.encode('hex')
        sys.exit(1)

    # handle the case of several handshake messages in one record
    leftover = r[4+len(body):]
    if len(leftover):
        parsehandshake(leftover)

# parses TLS record
def parserecord(r):
    global server_change_cipher_spec_received

    # read from TLS record header content type and length
    ctype = r[0]
    clength = bn(r[3:5])
    c = r[5:]

    # handle known types
    if ctype == "\x16":
        print "<--- handshake()"
        parsehandshake(c[:clength])
    elif ctype == "\x14":
        print "<--- change_cipher_spec()"
        server_change_cipher_spec_received = True
    elif ctype == "\x15":
        print "<--- alert()"
        level, desc = ord(c[0]), ord(c[1])
        if level == 1:
            print "	[-] warning:", desc
        elif level == 2:
            print "	[-] fatal:", desc
            sys.exit(1)
        else:
            sys.exit(1)
    elif ctype == "\x17":
        print "<--- application_data()"
        data = decrypt(c[:clength], "\x17", "\x03\x01")
        print data.strip()
    else:
        print "[-] Unknown TLS Record type:", ctype.encode('hex')
        sys.exit(1)

# PRF defined in TLS v1.0
def PRF(secret, label, seed, l):

    def xor(data, key):
        l = len(key)
        buff = ""
        for i in range(0, len(data)):
            buff += chr(ord(data[i]) ^ ord(key[i % l]))
        return buff

    def P_hash(secret, seed, alg, l):
        out = ""
        A = hmac.new(secret, seed, alg).digest()
        while len(out) < l:
            out += hmac.new(secret, A + seed, alg).digest()
            A = hmac.new(secret, A, alg).digest()
        out = out[:l]
        return out

    s1 = secret[:len(secret)/2]
    s2 = secret[len(secret)/2:len(secret)]
    return xor(P_hash(s1, label+seed, md5, l), P_hash(s2, label+seed, sha1, l))

# derives master_secret
def derive_master_secret():
    global premaster, master_secret, client_random, server_random
    master_secret = PRF(premaster, "master secret", client_random+server_random, 48)

# derives keys for encryption and MAC
def derive_keys():
    global premaster, master_secret, client_random, server_random
    global client_mac_key, server_mac_key, client_enc_key, server_enc_key, rc4c, rc4s

    key_block = PRF(master_secret, "key expansion", server_random+client_random, 136)
    mac_size = 20
    key_size = 16

    client_mac_key = key_block[:mac_size]
    server_mac_key = key_block[mac_size:mac_size*2]
    client_enc_key = key_block[mac_size*2:mac_size*2+key_size]
    server_enc_key = key_block[mac_size*2+key_size:mac_size*2+key_size*2]
    rc4c = ARC4.new(client_enc_key)
    rc4s = ARC4.new(server_enc_key)

# HMAC SHA1 wrapper
def HMAC_sha1(key, data):
    return hmac.new(key, data, sha1).digest()

# calculates MAC and encrypts plaintext
def encrypt(plain, type, version):
    global client_mac_key, client_enc_key, client_seq, rc4c

    mac = HMAC_sha1(client_mac_key, nb(client_seq, 8) + type + version + nb(len(plain), 2) + plain)
    ciphertext = rc4c.encrypt(plain + mac)
    client_seq+= 1
    return ciphertext

# decrypts ciphertext and verifies MAC
def decrypt(ciphertext, type, version):
    global server_mac_key, server_enc_key, server_seq, rc4s

    d = rc4s.decrypt(ciphertext)
    mac = d[-20:]
    plain = d[:-20]

    # verify MAC
    mac_calc = HMAC_sha1(server_mac_key, nb(server_seq, 8) + type + version + nb(len(plain), 2) + plain)
    if mac!=mac_calc:
        print "[-] MAC verification failed!"
        sys.exit(1)
    server_seq+= 1
    return plain

# read from the socket full TLS record
def readrecord():
    record = ""

    # read TLS record header (5 bytes)
    while len(record) < 5:
        record += s.recv(1)

    # find data length
    datalen = bn(record[3:5])

    # read TLS record body
    while len(record) < (5 + datalen):
        record+= s.recv(1)

    return record

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse.urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]


path = url.path

client_random = ""	# will hold client randomness
server_random = ""	# will hold server randomness
server_cert = ""	# will hold DER encoded server certificate
premaster = ""		# will hold 48 byte pre-master secret
master_secret = ""	# will hold master secret
handshake_messages = "" # will hold concatenation of handshake messages

# client/server keys and sequence numbers
client_mac_key = ""
server_mac_key = ""
client_enc_key = ""
server_enc_key = ""
client_seq = 0
server_seq = 0

# client/server RC4 instances
rc4c = ""
rc4s = ""

s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
server_change_cipher_spec_received = False
server_finished_received = False

while not server_hello_done_received:
    parserecord(readrecord())

s.send(client_key_exchange())
s.send(change_cipher_spec())
derive_master_secret()
derive_keys()
s.send(finished())

while not server_finished_received:
    parserecord(readrecord())

s.send(application_data("GET / HTTP/1.0\r\n\r\n"))
parserecord(readrecord())

print "[+] Closing TCP connection!"
s.close()
