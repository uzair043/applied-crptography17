#!/usr/bin/env python

import argparse, hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# It took me 6 hours.


# parse arguments
parser = argparse.ArgumentParser(description='generate self-signed X.509 CA certificate', add_help=False)
parser.add_argument("private_key_file", help="Private key file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store self-signed CA certificate (PEM form)")
args = parser.parse_args()


#-----------------------------------ASN.1 Encoder Functions----------------------------------#

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring_asn1(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring_asn1(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value


def asn1_bistring_bytestring(intRepr):
    type = chr(0x03)
    padding = 0

    value = int_to_bytestring_asn1(intRepr)

    length = asn1_len(chr(padding) + value)

    return type + length + chr(padding) + value



def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der




##############################################################################################
def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER

    if content.startswith("-----BEGIN RSA PRIVATE KEY-----"):
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")

    content = content.decode("base64")

    return content

def get_pubkey(filename):
    # reads private key file and returns (n, e)
    file_reader = open(filename, 'r')
    private_key_reader = file_reader.read()
    file_reader.close()

    if private_key_reader.startswith("-----"):
        private_key_reader = pem_to_der(private_key_reader)

    privkey = decoder.decode(private_key_reader)

    return int(privkey[0][1]), int(privkey[0][2])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    file_reader = open(filename, 'r')
    private_key_reader = file_reader.read()
    file_reader.close()

    if private_key_reader.startswith("-----"):
        private_key_reader = pem_to_der(private_key_reader)

    privkey = decoder.decode(private_key_reader)

    return int(privkey[0][1]), int(privkey[0][3])

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of modulus n
    size = 0
    while n:
        size = size + 1
        n = n >> 8

    plain_text_length = len(plaintext) + 3
    padding_length = size - plain_text_length

    # plaintext must be at least 3 bytes smaller than modulus
    padding = '\xFF' * padding_length

    # generate padding bytes
    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    padding_length = 0
    if plaintext[0] == '\x00':
        padding_length = 1
    else:
        return plaintext

    while plaintext[padding_length] != '\x00':
        padding_length += 1

    padding_length += 1

    plaintext = plaintext[padding_length:]
    return plaintext


def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of m
    SHA256 = hashlib.sha256()

    SHA256.update(m)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([2,16,840,1,101,3,4,2,1]) + asn1_null()) + asn1_octetstring(SHA256.digest()))
    return der

def sign(m, keyfile):
    # sign DigestInfo of message m
    n, d = get_privkey(keyfile)

    s = pow(m, d, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    c = int_to_bytestring(s, size)
    return c

def constructIssuer():
    C = 'EE'
    ON = 'University of Tartu'
    OU = 'Computer Science Department'
    CN = 'Muhammad Uzair'


    der = asn1_sequence(asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,6]) +
		  asn1_printablestring(C))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,10]) +
		  asn1_printablestring(ON))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,11]) +
		  asn1_printablestring(OU))) + asn1_set(asn1_sequence(asn1_objectidentifier([2,5,4,3]) +
		  asn1_printablestring(CN))))

    return der

def constructExtensions():
    encapsulation = asn1_sequence(asn1_boolean(True))
    return asn1_sequence(asn1_sequence(asn1_objectidentifier([2,5,29,15]) + asn1_octetstring(asn1_bitstring("0000011"))) + asn1_sequence(asn1_objectidentifier([2,5,29,19]) + asn1_boolean(True) + asn1_octetstring(encapsulation)))

def selfsigned(privkey, certfile):
    # create x509v3 self-signed CA root certificate
    serialNumber = 66059

    # get public key (n, e) from private key file
    n, e = get_pubkey(privkey)

    # construct subjectPublicKeyInfo from public key values (n, e)
    keyInfo = asn1_sequence(asn1_integer(n) + asn1_integer(e))
    keyBits = asn1_bistring_bytestring(bytestring_to_int(keyInfo))

    subjectPublicKeyInfo = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,1]) + asn1_null()) + keyBits)

    # construct tbsCertificate structure
    tbsCertificate = asn1_sequence(asn1_tag_explicit(asn1_integer(2), 0) + asn1_integer(serialNumber) + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,11]) + asn1_null()) + constructIssuer() + asn1_sequence(asn1_utctime("160828125931Z") + asn1_utctime("170828125931Z")) + constructIssuer() + subjectPublicKeyInfo + asn1_tag_explicit(constructExtensions(), 3))
    #print(str(decoder.decode(tbsCertificate)))

    # sign tbsCertificate structure
    paddedText = pkcsv15pad_sign(digestinfo_der(tbsCertificate), n)
    signature = sign(bytestring_to_int(paddedText), privkey)

    # construct final X.509 DER

    cert = asn1_sequence(tbsCertificate + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,11]) + asn1_null()) + asn1_bistring_bytestring(bytestring_to_int(signature)))

    # convert to PEM by .encode('base64') and adding PEM headers
    pem = '-----BEGIN CERTIFICATE-----\n' + cert.encode('base64') + '-----END CERTIFICATE-----'

    # write PEM certificate to file
    open(certfile, 'w').write(pem)

selfsigned(args.private_key_file, args.output_cert_file)
