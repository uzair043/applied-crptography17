#!/usr/bin/env python

import argparse, datetime, hashlib, re, socket, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# specify here how much time your solution required

# parse arguments
parser = argparse.ArgumentParser(description='OCSP responder', add_help=False)
parser.add_argument("--privkey", required=True, metavar='privkey', type=str, help="CA private key (DER/PEM)")
parser.add_argument("--cacert", required=True, metavar='cacert', type=str, help="CA certificate (DER/PEM)")
parser.add_argument("--revoked", required=True, metavar='cert', type=str, nargs='+', help="Revoked certificates (DER/PEM)")
args = parser.parse_args()

#-----------------------------------ASN.1 Encoder Functions----------------------------------#

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring_asn1(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring_asn1(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value

def asn1_enumerated(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of ENUMERATED
    type = chr(0x0A)
    value = int_to_bytestring_asn1(i)

    temp = i
    while temp > 255:
        temp = temp >> 8

    if temp >= 128:
        rep = chr(0x00) + value

    length = asn1_len(rep)

    return type + length + value


def asn1_bistring_bytestring(intRepr):
    type = chr(0x03)
    padding = 0

    value = int_to_bytestring_asn1(intRepr)

    length = asn1_len(chr(padding) + value)

    return type + length + chr(padding) + value



def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der

def asn1_generalizedtime(time):
    # returns DER encoding of UTCTime
    type = chr(0x18)
    length = asn1_len(time)
    return type + length + time



def asn1_tag_implicit(der, tag):
    form_bit = ord(der[0]) & 32
    type = chr(128 + form_bit + tag)
    return type + der[1:]

##############################################################################################



def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == '--':
        content = content.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
        content = content.replace("-----END CERTIFICATE REQUEST-----", "")
        content = content.replace("-----BEGIN CERTIFICATE-----", "")
        content = content.replace("-----END CERTIFICATE-----", "")
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")
        content = content.decode('base64')
    return content

def get_pubkey_cert(cert):
    # reads certificate and returns subjectPublicKey
    certificate = open(cert, 'r').read()

    if certificate.startswith("-----"):
        certificate = pem_to_der(certificate)

    certificate_dec = decoder.decode(certificate)

    public_key_bits = certificate_dec[0][0][6][1]

    octets_length = len(public_key_bits) / 8

    octets = 0

    for bit in public_key_bits:
        octets = octets << 1 | bit

    return int_to_bytestring(octets, octets_length)

def get_privkey(filename):
    # reads private key file and returns (n, d)
    private_key_read = open(filename, 'r').read()

    if private_key_read.startswith("-----"):
        private_key_read = pem_to_der(private_key_read)

    private_key = decoder.decode(private_key_read)

    return int(private_key[0][1]), int(private_key[0][3])


def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of file
    SHA1 = hashlib.sha1()

    SHA1.update(m)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) +
          asn1_null()) + asn1_octetstring(SHA1.digest()))

    return der

def sign(m, keyfile):
    n, d = get_privkey(keyfile)

    s = pow(m, d, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    c = int_to_bytestring(s, size)

    return c

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    size = 0

    while n:
        size = size + 1
        n = n >> 8

    text_length = len(plaintext) + 3
    padding_length = size - text_length

    padding = '\xFF' * padding_length

    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext

    return padded_plaintext

# serial numbers of revoked certificates
serials = []

# in a loop obtain DER encoded OCSP request and send response
def process_requests():

    sserv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sserv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sserv.bind(('', 8888))
    sserv.listen(0)

    while True:
        (s, address) = sserv.accept()
        print "[+] Connection from %s:%s" % (address[0], address[1])

        # read HTTP request header
        header = ""
        while True:
            buffer = s.recv(1)
            if not buffer:
                print "[-] Connection Closed - Program exiting..."
                sys.exit(1)
            header = header + buffer
            if header[-4:] == "\r\n\r\n":
                break;

        # send error message if GET request (bonus)
        if header.startswith('GET'):
            answer = "The server is processing only OCSP POST requests!"
            header = "HTTP/1.1 405 Not Allowed\r\n" + "Date: "+str(datetime.datetime.utcnow())+\
                     "\r\n" + "Connection: close\r\n" + "Content-Length: " + str(len(answer)) + "\r\n\r\n"
            s.send(header + answer)
            s.close()
            return

        # read OCSP request
        content_length = int(re.search('content-length:\s*(\d+)\s', header, re.S+re.I).group(1), 10)

        OCSP_request = ""
        for i in range(content_length):
            buffer = s.recv(1)
            if not buffer:
                print "[-] Connection Closed - Program exiting..."
                sys.exit(1)
            OCSP_request = OCSP_request + buffer

        # produce OCSP response
        OCSP_response = produce_response(OCSP_request, address[0])

        # prepend HTTP response header
        header = "HTTP/1.1 200 OK\r\n" + "Date: "+str(datetime.datetime.utcnow())+"\r\n" + \
                 "Connection: close\r\n" + "Content-Length: " + str(len(OCSP_response)) + "\r\n\r\n"
        # send the response
        answer = header + OCSP_response
        s.send(answer)
        s.close()

# load serials of revoked certificates to list 'serials'
def load_serials(certificates):
    global serials

    for certificate in certificates:
        cert = open(certificate, 'r').read()
        certificate_DER = pem_to_der(cert)

        serial = decoder.decode(certificate_DER)[0][0][1]

        print "[+] Serial %s (%s) loaded" % (serial, certificate)

        serials.append(serial)


def composeResponseData(name, cert_ID, cert_status, thisUpdate, nonce):
    if nonce == "":
        return asn1_sequence(asn1_tag_explicit(name, 1) +
               asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) +
               asn1_sequence(asn1_sequence(cert_ID + cert_status + thisUpdate)))
    else:
        return asn1_sequence(asn1_tag_explicit(name, 1) +
               asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) +
               asn1_sequence(asn1_sequence(cert_ID + cert_status + thisUpdate)) + nonce)

# produce OCSP response for DER encoded request
def produce_response(req, ip):
    global args, serials

    open("request", 'w').write(req)

    # return unauthorized(6) if non-localhost client (bonus)
    if '127.0.0.1' not in ip:
        return asn1_sequence(asn1_enumerated(6))

    # get subject name from CA certificate
    cacert = open(args.cacert).read()
    if cacert.startswith('-----'):
        cacert = pem_to_der(cacert)

    subject_name = encoder.encode(decoder.decode(cacert)[0][0][5])

    # get subjectPublicKey (not subjectPublicKeyInfo) from CA certificate
    public_key = get_pubkey_cert(args.cacert)

    # get serial number from request
    serial = decoder.decode(req)[0][0][0][0][0][3]

    # calculate SHA1 hash of CA subject name and CA public key (return CertStatus 'unknown' if not issued by CA)
    SHA1 = hashlib.sha1()
    SHA1.update(subject_name)
    subject_name_hash = SHA1.digest()

    issuer_name_hash = str(decoder.decode(req)[0][0][0][0][0][1])

    cert_status = ""

    if subject_name_hash != issuer_name_hash:
        cert_status = asn1_tag_implicit(asn1_null(), 2)

    SHA1 = hashlib.sha1()
    SHA1.update(public_key)
    public_key_hash = SHA1.digest()

    issuer_key_hash = str(decoder.decode(req)[0][0][0][0][0][2])

    if public_key_hash != issuer_key_hash:
        cert_status = asn1_tag_implicit(asn1_null(), 2)

    # return 'revoked' CertStatus if serial in serials (otherwise return 'good')
    if serial in serials:
        cert_status = asn1_tag_implicit(asn1_sequence(asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ")) +
                      asn1_tag_explicit(asn1_enumerated(1), 0)), 1)
    if cert_status == "":
        cert_status = asn1_tag_implicit(asn1_null(), 0)

    # nonce extension (bonus)
    nonce = ""

    if len(decoder.decode(req)[0][0]) == 2:
        nonce = asn1_tag_explicit(asn1_sequence(encoder.encode(decoder.decode(req)[0][0][1][0])), 1)

    name = encoder.encode(decoder.decode(cacert)[0][0][5])
    cert_ID = encoder.encode(decoder.decode(req)[0][0][0][0][0])
    thisUpdate = asn1_generalizedtime(datetime.datetime.utcnow().strftime("%Y%m%d%H%M%SZ"))

    tbsResponseData = composeResponseData(name, cert_ID, cert_status, thisUpdate, nonce)

    # get signature of tbsResponseData
    #n, d = get_privkey(args.privkey)
    #padded_text = pkcsv15pad_sign(digestinfo_der(tbsResponseData), n)
    signature = sign(tbsResponseData, args.privkey)

    resp = asn1_sequence(asn1_enumerated(0) + asn1_tag_explicit(asn1_sequence(asn1_objectidentifier([1,3,6,1,5,5,7,48,1,1]) +
           asn1_octetstring(asn1_sequence(tbsResponseData + asn1_sequence(asn1_objectidentifier([1,2,840,113549,1,1,5]) +
           asn1_null()) + asn1_bistring_bytestring(signature)))), 0))


    # return DER encoded OCSP response
    open("response_file", 'w').write(resp)
    return resp

load_serials(args.revoked)
process_requests()
