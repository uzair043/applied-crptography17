#!/usr/bin/python

import hashlib, sys
from pyasn1.codec.der import decoder
sys.path = sys.path[1:] # don't remove! otherwise the library import below will try to import your hmac.py 
import hmac # do not use any other imports/libraries

# It took me almost one day because at first my second homework was not 100% correct

#-------------------------------ASN.1 Encoder-------------------------#

def bytestring_to_int(s):
    # your implementation here
    i = 0
    for letter in  range(0, len(s) ):
        if(letter == 0):
            i = ord(s[letter])
        else:
            i = i << 8
            i = i | ord(s[letter])
    return i

def int_to_bytestring(i):
    byteString = ""
    while i:
    	n = i & 0xff
    	byteString = chr(n) + byteString
    	i = i >> 8

    if byteString == "":
        byteString = chr(0x00)
    return byteString


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der



#---------------------------------------------------------------------#
def verify(filename):
    print "[+] Reading HMAC DigestInfo from", filename+".hmac"

    hash = hmac.new("")

    d_read = ""
    f = open(filename+".hmac" , 'rb')
    data = f.read(512)

    while data:
        d_read = d_read + data
        data = f.read(512)


    d_decode = decoder.decode(d_read)
    a_identifier = (d_decode[0])[0]
    o_identifier = str(a_identifier[0])
    digest = (d_decode[0])[1]

    #--Check if the object identifier is SHA-1
    if o_identifier == "1.3.14.3.2.26":
        print "[+] HMAC-SHA-1 digest: " + hex(bytestring_to_int(digest))

    #--Check if the object identifier is SHA-2
    if o_identifier == "2.16.840.1.101.3.4.2.1":
        print "[+] HMAC-SHA-2 digest: " + hex(bytestring_to_int(digest))

    #--Check if the object identifier is MD5
    if o_identifier == "1.2.840.113549.2.5":
        print "[+] HMAC-MD5 digest: " + hex(bytestring_to_int(digest))


    print "[?] Enter key:"
    key = raw_input()

    f = open(filename, 'rb')
    data = f.read(512)


    #--Calculating SHA-1
    if o_identifier == "1.3.14.3.2.26":
        hash = hmac.new(key, None, hashlib.sha1)
        while data:
            hash.update(data)
            data = f.read(512)

        print "[+] Calculated HMAC-SHA-1: " + hash.hexdigest()


    #--Calculating SHA-2
    if o_identifier == "2.16.840.1.101.3.4.2.1":
        hash = hmac.new(key, None, hashlib.sha256)
        while data:
            hash.update(data)
            data = f.read(512)

        print "[+] Calculated HMAC-SHA-2: " + hash.hexdigest()

     #--Calculating MD5
    if o_identifier == "1.2.840.113549.2.5":
        hash = hmac.new(key, None, hashlib.md5)
        while data:
            hash.update(data)
            data = f.read(512)

        print "[+] Calculated HMAC-MD5: " + hash.hexdigest()

    digest_calculated = hash.digest()

    if digest_calculated != digest:
        print "[-] Wrong key or message has been manipulated!"
    else:
        print "[+] HMAC verification successful!"

def mac(filename):
    print "[?] Enter key:",
    key = raw_input()

    hash = hmac.new(key, None, hashlib.sha256)

    f = open(filename, 'rb')
    data = f.read(512)

    while data:
        hash.update(data)
        data = f.read(512)

    print "[+] Calculated HMAC-256: " + hash.hexdigest()

    asn1 = asn1_sequence(asn1_sequence(asn1_objectidentifier([2,16,840,1,101,3,4,2,1]) + asn1_null()) +
            asn1_octetstring(hash.digest()))


    print "[+] Writing HMAC DigestInfo to", filename+".hmac"

    open(filename+".hmac" , 'w').write(asn1)


def usage():
    print "Usage:"
    print "-verify <filename>"
    print "-mac <filename>"
    sys.exit(1)

if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '-mac':
    mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()