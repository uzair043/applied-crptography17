#!/usr/bin/env python

import hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder

# It took me about 10-12 hours approximately.

#-------------------------------- ASN.1 Encoder -----------------------------------------#

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring_asn1(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring_asn1(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value

def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der



########################################################################################

def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER

    #private key
    if content.startswith("-----BEGIN RSA PRIVATE KEY-----"):
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")

    # public key
    if content.startswith("-----BEGIN PUBLIC KEY-----"):
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")

    content = content.decode("base64")

    return content

def get_pubkey(filename):
    # reads public key file and returns (n, e)
    public_key_file_reader = open(filename, 'r')
    pub_key_read = public_key_file_reader.read()
    public_key_file_reader.close()

    if pub_key_read.startswith("-----"):
        pub_key_read = pem_to_der(pub_key_read)

    der_representation = decoder.decode(pub_key_read)
    bit_string = der_representation[0][1]

    octets_length = len(bit_string) / 8
    octets = 0

    for bit in bit_string:
        octets = octets << 1 | bit

    pubkey = decoder.decode(int_to_bytestring(octets, octets_length))[0]

    return int(pubkey[0]), int(pubkey[1])

def get_privkey(filename):
    # reads private key file and returns (n, d)
    private_key_file_reader = open(filename, 'r')
    private_key_read = private_key_file_reader.read()
    private_key_file_reader.close()

    if private_key_read.startswith("-----"):
        private_key_read = pem_to_der(private_key_read)

    privkey = decoder.decode(private_key_read)

    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate byte size of the modulus n
    size = 0
    while n:
        size = size + 1
        n = n >> 8

    plain_text_length = len(plaintext) + 3 + 8
    additional_padding = size - plain_text_length
    padding_length = 8 + additional_padding

    # plaintext must be at least 11 bytes smaller than modulus
    random_padding = os.urandom(padding_length)

    while '\x00' in random_padding:
        random_padding = random_padding.replace('\x00', os.urandom(1))

    # generate padding bytes
    padded_plaintext = '\x00' + '\x02' + random_padding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of modulus n
    size = 0
    while n:
        size = size + 1
        n = n >> 8

    plain_text_length = len(plaintext) + 3
    padding_length = size - plain_text_length

    # plaintext must be at least 3 bytes smaller than modulus
    padding = '\xFF'*padding_length

    # generate padding bytes
    padded_plaintext = '\x00' + '\x01' + padding + '\x00' + plaintext
    return padded_plaintext

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    padding_length = 0
    if plaintext[0] == '\x00':
        padding_length = 1
    else:
        return plaintext

    while plaintext[padding_length] != '\x00':
        padding_length += 1

    padding_length += 1

    plaintext = plaintext[padding_length:]

    return plaintext

def encrypt(keyfile, plaintextfile, ciphertextfile):
    n, e = get_pubkey(keyfile)

    plain_text_file_reader = open(plaintextfile, 'r')
    plain_text_read = plain_text_file_reader.read()

    padded_text = pkcsv15pad_encrypt(plain_text_read, n)

    m = bytestring_to_int(padded_text)

    c = pow(m, e, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    cipher_text = int_to_bytestring(c, size)

    cipher_text_file_writer = open(ciphertextfile, 'w')
    cipher_text_file_writer.write(cipher_text)

    pass

def decrypt(keyfile, ciphertextfile, plaintextfile):
    n, d = get_privkey(keyfile)

    cipher_text_file_reader = open(ciphertextfile, 'r')
    cipher_text_read = cipher_text_file_reader.read()

    c = bytestring_to_int(cipher_text_read)

    m = pow(c, d, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    padded_text = int_to_bytestring(m, size)

    plain_text = pkcsv15pad_remove(padded_text)

    plain_text_file_writer = open(plaintextfile, 'w')
    plain_text_file_writer.write(plain_text)

    pass

def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of file
    sha256 = hashlib.sha256()

    file_reader = open(filename, 'rb')
    data = file_reader.read(512)

    while data:
        sha256.update(data)
        data = file_reader.read(512)

    der = asn1_sequence(asn1_sequence(asn1_objectidentifier([1,3,14,3,2,26]) + asn1_null()) + asn1_octetstring(sha256.digest()))
    return der

def sign(keyfile, filetosign, signaturefile):
    n, d = get_privkey(keyfile)

    plain_text = digestinfo_der(filetosign)

    padded_text = pkcsv15pad_sign(plain_text, n)

    m = bytestring_to_int(padded_text)

    s = pow(m, d, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    signature = int_to_bytestring(s, size)

    signature_file_writer = open(signaturefile, 'w')
    signature_file_writer.write(signature)
    pass

def verify(keyfile, signaturefile, filetoverify):
    # prints "Verified OK" or "Verification Failure"
    n, e = get_pubkey(keyfile)

    signature_file_reader = open(signaturefile, 'rb')
    signature = signature_file_reader.read()

    s = bytestring_to_int(signature)

    m = pow(s, e, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    digest_info_padded = int_to_bytestring(m, size)
    digest_info = decoder.decode(pkcsv15pad_remove(digest_info_padded))

    object_identifier = str(digest_info[0][0][0])
    digestSHA256 = digest_info[0][1]

    if object_identifier == "2.16.840.1.101.3.4.2.1":
        sha256 = hashlib.sha256()

        file_read_verify = open(filetoverify, 'rb')
        data = file_read_verify.read(512)

        while data:
            sha256.update(data)
            data = file_read_verify.read(512)

        computedSHA1 = sha256.digest()

        if digestSHA256 == computedSHA1:
            print "Verified OK"
        else:
            print "Verification Failure"

    else:
        print "Verification Failure"

    pass

def usage():
    print "Usage:"
    print "encrypt <public key file> <plaintext file> <output ciphertext file>"
    print "decrypt <private key file> <ciphertext file> <output plaintext file>"
    print "sign <private key file> <file to sign> <signature output file>"
    print "verify <public key file> <signature file> <file to verify>"
    sys.exit(1)

if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
