#!/usr/bin/env python

import argparse, hashlib, datetime, re, socket, sys, os, urlparse  # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# specify here how much time your solution required

# parse arguments
parser = argparse.ArgumentParser(description='Check certificates against CRL', add_help=False)
parser.add_argument("url", type=str, help="URL of CRL (DER)")
parser.add_argument("--issuer", required=True, metavar='issuer', type=str, help="CA certificate that has issued certificates (DER/PEM)")
parser.add_argument("--certificates", required=True, metavar='cert', type=str, nargs='+', help="Certificates to check if revoked (DER/PEM)")
args = parser.parse_args()


#-----------------------------------ASN.1 Encoder Functions----------------------------------#

def int_to_bytestring_asn1(i):
    s = ""
    while i:
        n = i & 0xff
        s = chr(n) + s
        i = i >> 8

    if s == "":
        s = chr(0x00)
    return s


def asn1_len(content_str):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    length = len(content_str)
    if length <= 127:
        return chr(length)
    else:
        lengthBytes = int_to_bytestring_asn1(length)
        return chr(128 | len(lengthBytes)) + lengthBytes

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_null():
    # returns DER encoding of NULL
    return chr(0x05) + chr(0x00)

def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    type = chr(0x02)
    value = int_to_bytestring_asn1(i)
    temp = i
    while temp > 255:
        temp = temp >> 8
    if temp >= 128:
        value = chr(0x00) + value

    length = asn1_len(value)

    return type + length + value

def asn1_bitstring(bitstr):
    # bitstr - bytestring containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    type = chr(0x03)
    rightpadding = 8 - (len(bitstr) % 8)
    if rightpadding == 8:
        rightpadding = 0
    bitstr = bitstr + (rightpadding*"0")

    if bitstr == "":
        value = ""
    else:
        value = int_to_bytestring_asn1(int(bitstr, 2))

    length = asn1_len(chr(rightpadding) + value)

    return type + length + chr(rightpadding) + value


def asn1_bistring_bytestring(intRepr):
    type = chr(0x03)
    padding = 0

    value = int_to_bytestring_asn1(intRepr)

    length = asn1_len(chr(padding) + value)

    return type + length + chr(padding) + value



def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., "abc\x01")
    # returns DER encoding of OCTETSTRING
    type = chr(0x04)
    length = asn1_len(octets)
    return type + length + octets

def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    type = chr(0x06)
    value = ""
    if len(oid) >= 2:
        value = value + chr(oid[0] * 40 + oid[1])
    elif len(oid >= 1):
        value = value + chr(oid[0] * 40 )
    else:
        value = value + chr(0x00)

    for i in range(2, len(oid)):
        num = oid[i]
        for j in range(10, 0, -1):
            power = pow(128,j)
            if num >= power:
                value = value + chr(128 + (num / power))
                num = num - (num / power) * power
        value = value + chr(oid[i] % 128)

    length = asn1_len(value)

    return type + length + value

def asn1_sequence(der):
    # der - DER bytestring to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    type = chr(0x30)
    length = asn1_len(der)

    return type + length + der

def asn1_set(der):
    # der - DER bytestring to encapsulate into set
    # returns DER encoding of SET
    type = chr(0x31)
    length = asn1_len(der)

    return type + length + der

def asn1_printablestring(string):
    # string - bytestring containing printable characters (e.g., "foo")
    # returns DER encoding of PrintableString
    type = chr(0x13)
    length = asn1_len(string)

    return type + length + string

def asn1_utctime(time):
    # time - bytestring containing timestamp in UTCTime format (e.g., "121229010100Z")
    # returns DER encoding of UTCTime
    type = chr(0x17)
    length = asn1_len(time)
    return type + length + time

def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    type = chr(160 + tag)
    length = asn1_len(der)
    return type + length + der




################################################################################



def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == '--':
        content = content.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
        content = content.replace("-----END CERTIFICATE REQUEST-----", "")
        content = content.replace("-----BEGIN CERTIFICATE-----", "")
        content = content.replace("-----END CERTIFICATE-----", "")
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")
        content = content.decode('base64')
    return content

def get_pubkey_certificate(filename):
    # reads certificate and returns (n, e) from subject public key
    file_reader = open(filename, 'r')
    certificate = file_reader.read()
    file_reader.close()

    if certificate.startswith("-----"):
        certificate = pem_to_der(certificate)

    certificate_rep = decoder.decode(certificate)
    public_key_bits = certificate_rep[0][0][6][1]
    octets_length = len(public_key_bits) / 8
    octets = 0

    for bits in public_key_bits:
        octets = octets << 1 | bits

    public_key_rep = decoder.decode(int_to_bytestring(octets, octets_length))[0]

    return int(public_key_rep[0]) , int(public_key_rep[1])


def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    plaintext = plaintext[2:]
    if "\x00" in plaintext:
        return plaintext[plaintext.index("\x00")+1:]

def digestinfo_der(m, alg_oid):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA-X digest of m
    digest = ""
    hash_oid = []

    if alg_oid == [1,2,840,113549,1,1,5]:
        digest = hashlib.sha1(m).digest()
        hash_oid = [1,3,14,3,2,26]
    elif alg_oid == [1,2,840,113549,1,1,11]:
        digest = hashlib.sha256(m).digest()
        hash_oid = [2,16,840,1,101,3,4,2,1]
    elif alg_oid == [1,2,840,113549,1,1,12]:
        digest = hashlib.sha384(m).digest()
        hash_oid = [2,16,840,1,101,3,4,2,2]
    elif alg_oid == [1,2,840,113549,1,1,13]:
        digest = hashlib.sha384(m).digest()
        hash_oid = [2,16,840,1,101,3,4,2,3]
    else:
        print "[-] digestinfo_der(): unrecognized alg_oid:", alg_oid
        sys.exit(1)

    digestinfo = asn1_sequence( \
        asn1_sequence( \
                asn1_objectidentifier(hash_oid) + \
                asn1_null()) + \
        asn1_octetstring(digest))

    return digestinfo

def verify(certfile, c, contenttoverify, alg_oid):
    # returns 1 on "Verified OK" and 0 otherwise
    n, e = get_pubkey_certificate(certfile)

    m = pow(c, e, n)

    size = 0
    while n:
        size = size + 1
        n = n >> 8

    m = int_to_bytestring(m, size)
    m = pkcsv15pad_remove(m)

    digestinfo_from_content = digestinfo_der(contenttoverify, alg_oid)
    digestinfo_from_signature = digestinfo_der(certfile, alg_oid)


    if digestinfo_from_signature == digestinfo_from_content:
        return 1
    return 0


# list of serial numbers to check
serials = []

# download CRL using python sockets
def download_crl(url):

    print "[+] Downloading", url

    # parsing url
    parse = urlparse.urlparse(url)
    netloc = parse.netloc
    path = parse.path

    # get response header
    request_string = "GET " + path + " HTTP/1.1\r\nHost: " + netloc + "\r\n\r\n"

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((netloc, 80))
    s.send(request_string)
    header = ""

    while True:
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        header = header + buff
        if header[-4:] == "\r\n\r\n":
            break;

    # get content-length value
    content_length = int(re.search('content-length:\s*(\d+)\s', header, re.S+re.I).group(1), 10)

    # receive CRL (get response body)
    crl = ""
    for i in range(content_length):
        buff = s.recv(1)
        if not buff:
            print "[-] Connection Closed - Program exiting..."
            sys.exit(1)
        crl = crl + buff

    return crl

# verify if the CRL is signed by the issuer and whether the CRL is fresh
def verify_crl(issuer, crl):

    tbsCertList = encoder.encode(decoder.decode(crl)[0][0])
    signature_bits = decoder.decode(crl)[0][2]
    octets_length = len(signature_bits) / 8
    signature = 0

    for bits in signature_bits:
        signature = signature << 1 | bits




    if verify(issuer, signature, tbsCertList, alg_oid):
        print "[+] CRL signature check successful!"
    else:
        print "[-] CRL signature verification failed!"
        sys.exit(1)


    thisUpdate = datetime.datetime.strptime(str(decoder.decode(crl)[0][0][3]), '%y%m%d%H%M%SZ')
    nextUpdate = datetime.datetime.strptime(str(decoder.decode(crl)[0][0][4]), '%y%m%d%H%M%SZ')
    now = datetime.datetime.utcnow()


    if now < thisUpdate or now > nextUpdate:
        print "[-] CRL outdated (nextUpdate: %s) (now: %s)" % (nextUpdate, now)
        sys.exit(1)


# verify if the certificates are signed by the issuer and add them to the list 'serials'
def load_serials(issuer, certificates):
    global serials

    for certificate in certificates:


        if verify(issuer, signature, tbsCertificate, alg_oid):
            print "[+] Serial %s (%s) loaded" % (serial, certificate)
            serials.append(serial)
        else:
            print "[-] Serial %s (%s) not loaded: not issued by CA" % (serial, certificate)


# check if the certificates are revoked
# if revoked -- print revocation date and reason (if available)
def check_revoked(crl):
    global serials

    CRLReason = {
	0: 'unspecified',
	1: 'keyCompromise',
	2: 'cACompromise',
	3: 'affiliationChanged',
	4: 'superseded',
	5: 'cessationOfOperation',
	6: 'certificateHold',
	8: 'removeFromCRL',
	9: 'privilegeWithdrawn',
	10: 'aACompromise',
	}

    # loop over revokedCerts
    for revokedCert in decoder.decode(crl)[0][0][5]:


        # if revoked serial is in our interest
        if serial in serials:


            # check if the extensions are present

                print "[-] Certificate %s revoked: %s %s" % (serial, revocationDate, reason)




# download CRL
crl = download_crl(args.url)

# verify CRL
verify_crl(args.issuer, crl)

# load serial numbers from valid certificates
load_serials(args.issuer, args.certificates)

# check revocation status
check_revoked(crl)
